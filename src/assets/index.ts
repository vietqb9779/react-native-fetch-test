import {IRewardItem} from '../types';
import BackIcon from './svg/chevron-left.svg';
import RightIcon from './svg/chevron-right.svg';
import InsufficientIcon from './svg/insufficient-icon.svg';
import HomeIcon from './svg/home-icon.svg';
import NotifyIcon from './svg/notify-icon.svg';
import CardIcon from './svg/card-icon.svg';
import PersonIcon from './svg/person-icon.svg';
import {Sizes} from '../theme';

export const IconSvg = {
  BackIcon,
  RightIcon,
  InsufficientIcon,
  HomeIcon,
  NotifyIcon,
  CardIcon,
  PersonIcon,
};

export const Images = {
  PetrolA: require('./image/1a.png'),
  PetrolB: require('./image/1b.png'),
  RentalA: require('./image/2a.png'),
  RentalB: require('./image/2b.png'),
  FoodA: require('./image/3a.png'),
  FoodB: require('./image/3b.png'),
};

export const COIN_USER = 340;
export const PADDING_COVER = 120;
export const PADDING_FLAT = PADDING_COVER * 2 + 20;
export const HEIGHT_Y = Sizes.wh * 0.55;

export const REWARDS_DATA: IRewardItem[] = [
  {
    title: 'Petrol',
    data: [
      {
        cover: Images.PetrolA,
        coin: 15,
        content: '50% discount for every $100 top-up on your Shell Petrol Card',
      },
      {
        cover: Images.PetrolB,
        coin: 1000,
        content: '70% discount top-up on your Shell Petrol Card',
      },
    ],
  },

  {
    title: 'Rental Rebate',
    data: [
      {
        cover: Images.PetrolA,
        coin: 20,
        content: 'Get $20 Rental rebate',
      },
      {
        cover: Images.PetrolB,
        coin: 15,
        content: 'Get $500 Rental rebate',
      },
    ],
  },

  {
    title: 'Food and Beverage',
    data: [
      {
        cover: Images.FoodA,
        coin: 25,
        content: 'NTUC Fairprice $50 Voucher',
      },
      {
        cover: Images.FoodB,
        coin: 5,
        content: 'Free Cold Stone Sundae at any flavour!',
      },
    ],
  },
];
