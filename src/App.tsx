import {
  Animated,
  FlatList,
  ListRenderItemInfo,
  StatusBar,
  StyleSheet,
  View,
} from 'react-native';
import React, {useRef, useState} from 'react';
import NavigationBar from './components/NavigationBar';
import Cover from './components/Cover';
import ListHeaderComponent from './components/ListHeaderComponent';
import {HEIGHT_Y, PADDING_FLAT, REWARDS_DATA} from './assets';
import ListItemReward from './components/ListItemReward';
import {IRewardItem} from './types';
import BottomTab from './components/BottomTab';
import {Colors} from './theme';

const StatusBarAnim = Animated.createAnimatedComponent(StatusBar);

const App = () => {
  const animatedValue = useRef(new Animated.Value(0)).current;
  const [light, setLight] = useState(false);

  const backgroundColorAnim = animatedValue.interpolate({
    inputRange: [0, HEIGHT_Y],
    outputRange: [Colors.grey, Colors.white],
  });

  const renderItem = ({item}: ListRenderItemInfo<IRewardItem>) => {
    return <ListItemReward data={item} />;
  };

  return (
    <View style={styles.container}>
      <StatusBarAnim
        animated={true}
        backgroundColor={backgroundColorAnim}
        barStyle={light ? 'light-content' : 'dark-content'}
      />
      <NavigationBar animatedValue={animatedValue} />
      <Cover animatedValue={animatedValue} />
      <FlatList
        onScroll={e => {
          const offsetY = e.nativeEvent.contentOffset.y;
          animatedValue.setValue(offsetY);

          if (offsetY > HEIGHT_Y && light) {
            setLight(false);
          }
          if (offsetY < HEIGHT_Y && !light) {
            setLight(true);
          }
        }}
        ListHeaderComponent={ListHeaderComponent}
        data={REWARDS_DATA}
        keyExtractor={(item, index) => `list-${index}`}
        renderItem={renderItem}
        contentContainerStyle={styles.flatList}
      />
      <BottomTab />
    </View>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  flatList: {
    marginTop: PADDING_FLAT,
    paddingBottom: PADDING_FLAT,
  },
});
