import {Dimensions, StyleSheet} from 'react-native';

export enum Colors {
  black = '#000000',
  white = '#FFFFFF',
  gray = '#92929D',
  grey = '#171725',
  blue = '#0062FF',
  textHint = '#B5B5BE',
  transparent = 'transparent',
  border = '#E5E5E5',
  insufficient = '#696974',
  danger = '#FC5A5A',
}

export enum Sizes {
  tiny = 4,
  xs = 8,
  sm = 12,
  md = 16,
  lg = 24,
  xl = 32,
  ww = Dimensions.get('screen').width,
  wh = Dimensions.get('screen').height,
}
export enum FontSizes {
  xs = 12,
  sm = 14,
  md = 16,
  lg = 18,
  xl = 24,
  xxl = 32,
}

export const StyleCommon = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  shadow: {
    shadowColor: 'rgba(0,0,0,0.2)',

    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,

    elevation: 6,
  },

  textNormal: {
    fontSize: FontSizes.md,
    color: Colors.gray,
    lineHeight: FontSizes.xl,
  },
});
