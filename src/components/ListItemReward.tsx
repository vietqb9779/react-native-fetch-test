import {Image, ScrollView, StyleSheet, View} from 'react-native';
import React from 'react';
import {IReward, IRewardItem} from '../types';
import {Colors, FontSizes, Sizes, StyleCommon} from '../theme';
import {COIN_USER, IconSvg} from '../assets';
import Text from './Text';

interface IProps {
  data: IRewardItem;
}

const ListItemReward = ({data}: IProps) => {
  const renderItem = (item: IReward, index: Number) => {
    const isInsufficient = COIN_USER < item.coin;
    return (
      <View style={styles.itemContainer} key={index.toString() + data.title}>
        <Image source={item.cover} style={styles.cover} />
        <View style={styles.itemContent}>
          <View style={[StyleCommon.row, {marginBottom: Sizes.tiny}]}>
            {isInsufficient && <IconSvg.InsufficientIcon />}
            <Text
              style={[
                styles.txtCoin,
                isInsufficient
                  ? {color: Colors.insufficient, marginLeft: Sizes.tiny}
                  : {},
              ]}>
              {item.coin} Coins
            </Text>
          </View>
          <Text style={StyleCommon.textNormal}>{item.content}</Text>

          {isInsufficient && (
            <Text style={styles.txtInsufficient}>{'Insufficient coins'}</Text>
          )}
        </View>
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <Text style={styles.txtTitle}>{data.title}</Text>
      <ScrollView
        showsHorizontalScrollIndicator={false}
        horizontal
        contentContainerStyle={[StyleCommon.row, styles.scrollView]}>
        {data.data.map(renderItem)}
      </ScrollView>
    </View>
  );
};

export default ListItemReward;

const WIDTH = 220;
const HEIGHT = 240;

const styles = StyleSheet.create({
  container: {
    paddingVertical: Sizes.lg,
    backgroundColor: Colors.white,
  },
  txtTitle: {
    paddingHorizontal: Sizes.lg,
    fontSize: FontSizes.lg,
    fontWeight: '600',
    color: Colors.grey,
  },

  scrollView: {
    marginTop: Sizes.lg,
    paddingRight: Sizes.lg,
  },

  itemContainer: {
    width: WIDTH,
    height: HEIGHT,
    marginLeft: Sizes.lg,
    marginVertical: Sizes.sm,
    borderRadius: 4,
    backgroundColor: Colors.white,

    ...StyleCommon.shadow,
  },

  itemContent: {
    padding: Sizes.md,
  },

  cover: {
    width: WIDTH,
    borderTopRightRadius: 4,
    borderTopLeftRadius: 4,
  },

  txtCoin: {
    fontWeight: '600',
    fontSize: FontSizes.md,
    color: Colors.blue,
  },

  txtInsufficient: {
    color: Colors.blue,
    fontWeight: '400',
    marginTop: Sizes.sm,
  },
});
