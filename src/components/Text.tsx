import {StyleSheet, Text as TextRN, TextProps} from 'react-native';
import React from 'react';

interface IProps extends TextProps {}

const Text = (props: IProps) => {
  const {style = {}} = props;
  return (
    <TextRN {...props} style={[style, styles.text]}>
      {props.children}
    </TextRN>
  );
};

export default Text;

const styles = StyleSheet.create({
  text: {
    fontFamily: 'Hellix',
  },
});
