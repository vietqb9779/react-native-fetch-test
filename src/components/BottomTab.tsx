import {StyleSheet, View} from 'react-native';
import React from 'react';
import {Colors, Sizes} from '../theme';
import {IconSvg} from '../assets';

const BottomTab = () => {
  const renderBadge = () => {
    return <View style={styles.badge} />;
  };
  return (
    // Should use bottom safe view
    <View style={styles.container}>
      <IconSvg.HomeIcon />
      <View>
        {renderBadge()}
        <IconSvg.NotifyIcon />
      </View>
      <IconSvg.CardIcon />
      <IconSvg.PersonIcon />
    </View>
  );
};

export default React.memo(BottomTab);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',

    borderTopColor: Colors.border,
    borderTopWidth: 1,
    backgroundColor: Colors.white,
    paddingTop: Sizes.md,
    paddingBottom: Sizes.md,
  },

  badge: {
    height: 12,
    width: 12,
    backgroundColor: Colors.danger,
    borderRadius: 6,
    position: 'absolute',
    right: 5,
    top: 4,
    zIndex: 1,
  },
});
