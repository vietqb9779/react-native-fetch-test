import {SafeAreaView, StyleSheet, View, Animated} from 'react-native';
import React from 'react';
import {HEIGHT_Y, IconSvg} from '../assets';
import {Colors, FontSizes, Sizes, StyleCommon} from '../theme';
import Text from './Text';

interface IProps {
  animatedValue: Animated.Value;
}

const NavigationBar = ({animatedValue}: IProps) => {
  const opacityStyle = {
    opacity: animatedValue.interpolate({
      inputRange: [0, HEIGHT_Y],
      outputRange: [0, 1],
      extrapolate: 'clamp',
    }),
  };

  const backgroundColorAnim = animatedValue.interpolate({
    inputRange: [0, HEIGHT_Y],
    outputRange: [Colors.transparent, Colors.white],
  });

  const borderColorAnim = animatedValue.interpolate({
    inputRange: [0, HEIGHT_Y],
    outputRange: [Colors.transparent, Colors.border],
  });

  return (
    <Animated.View
      style={[
        styles.container,
        {
          backgroundColor: backgroundColorAnim,
          borderBottomColor: borderColorAnim,
        },
      ]}>
      <SafeAreaView
        style={[StyleCommon.row, {justifyContent: 'space-between'}]}>
        <View style={styles.btnBack}>
          <IconSvg.BackIcon height={15} width={15} />
        </View>

        <Animated.View style={[styles.btnView, opacityStyle]}>
          <Text style={styles.btnText}>My Coupons</Text>
        </Animated.View>
      </SafeAreaView>
    </Animated.View>
  );
};

export default React.memo(
  NavigationBar,
  (prev, next) => prev.animatedValue === next.animatedValue,
);

const BTN_SIZE = 40;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    zIndex: 1,
    position: 'absolute',
    paddingHorizontal: Sizes.lg,
    paddingVertical: Sizes.sm,
  },
  btnBack: {
    backgroundColor: Colors.white,
    height: BTN_SIZE,
    width: BTN_SIZE,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: BTN_SIZE / 2,
    borderWidth: 0.5,
    borderColor: Colors.border,
  },
  btnView: {
    backgroundColor: Colors.grey,
    paddingVertical: Sizes.sm,
    paddingHorizontal: Sizes.md,
    borderRadius: 4,
  },
  btnText: {
    color: Colors.white,
    fontWeight: '500',
    fontSize: FontSizes.xs,
  },
});
