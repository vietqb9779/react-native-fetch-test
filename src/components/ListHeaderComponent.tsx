import {StyleSheet, View} from 'react-native';
import React from 'react';
import {Colors, FontSizes, Sizes, StyleCommon} from '../theme';
import {COIN_USER, IconSvg} from '../assets';
import Text from './Text';

const ListHeaderComponent = () => {
  const renderStatus = () => {
    return (
      <View style={styles.viewStatus}>
        <View
          style={[
            styles.viewStatus,
            {backgroundColor: Colors.blue, width: '69%'},
          ]}
        />
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <Text style={styles.txtTitle}> {'Available Coin balance'}</Text>
      <Text style={styles.textCoin}>{COIN_USER}</Text>
      {renderStatus()}
      <Text style={StyleCommon.textNormal}>
        {
          'You have paid rental fee for $1,200. Pay more $800 to achieve Gold Tier.'
        }
      </Text>
      <View style={[StyleCommon.row, styles.viewBenefit]}>
        <Text style={styles.txtBenefit}>{'View tier benefits'}</Text>
        <IconSvg.RightIcon width={12} height={16} />
      </View>

      <View style={styles.btnView}>
        <Text style={styles.btnText}>{'My Coupons'}</Text>
      </View>

      <Text style={styles.txtUpdate}>{'Updated : 02/11/2021'}</Text>
    </View>
  );
};

export default ListHeaderComponent;

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    borderRadius: 8,
    padding: Sizes.lg,
    marginHorizontal: Sizes.md,
    marginBottom: Sizes.md,

    ...StyleCommon.shadow,
  },

  txtTitle: {
    fontWeight: '600',
    color: Colors.textHint,
    lineHeight: FontSizes.xl,
  },

  textCoin: {
    fontSize: 48,
    fontWeight: '400',
    color: Colors.grey,
    marginVertical: Sizes.lg,
  },

  txtBenefit: {
    fontWeight: '400',
    fontSize: FontSizes.md,
    color: Colors.blue,
    marginRight: Sizes.xs,
  },
  viewBenefit: {
    paddingVertical: Sizes.lg,
  },

  viewStatus: {
    height: 5,
    backgroundColor: '#E2E2EA',
    borderRadius: 2.5,
    marginBottom: Sizes.xl,
  },

  btnView: {
    backgroundColor: Colors.grey,
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
    borderRadius: 4,
  },
  btnText: {
    color: Colors.white,
    fontWeight: '600',
    fontSize: FontSizes.lg,
  },

  txtUpdate: {
    fontWeight: '400',
    color: Colors.textHint,
    textAlign: 'center',
    marginTop: Sizes.lg,
  },
});
