import {Animated, StyleSheet} from 'react-native';
import React from 'react';
import {Colors, FontSizes, Sizes} from '../theme';
import {HEIGHT_Y, PADDING_COVER} from '../assets';

interface IProps {
  animatedValue: Animated.Value;
}

const Cover = ({animatedValue}: IProps) => {
  const fontTitleAnim = {
    fontSize: animatedValue.interpolate({
      inputRange: [-HEIGHT_Y, 0],
      outputRange: [100, FontSizes.xxl],
      extrapolate: 'clamp',
    }),
  };

  const fontDescAnim = {
    fontSize: animatedValue.interpolate({
      inputRange: [-HEIGHT_Y, 0],
      outputRange: [FontSizes.xl, FontSizes.md],
      extrapolate: 'clamp',
    }),
  };

  return (
    <Animated.View style={[styles.container]}>
      <Animated.Text style={[styles.txtTitle, fontTitleAnim]}>
        {'Silver Tier'}
      </Animated.Text>
      <Animated.Text style={[styles.txtDesc, fontDescAnim]}>
        {
          'In Silver Tier, every $1 in rental fee paid, you get 2 coins to redeem exclusive rewards.'
        }
      </Animated.Text>
    </Animated.View>
  );
};

export default React.memo(
  Cover,
  (prev, next) => prev.animatedValue === next.animatedValue,
);

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    height: Sizes.ww,
    width: Sizes.ww,
    backgroundColor: Colors.grey,
    paddingTop: PADDING_COVER,
    paddingHorizontal: Sizes.lg,
  },

  txtTitle: {
    color: Colors.white,
    fontWeight: '600',
    paddingBottom: Sizes.xs,
    fontFamily: 'Hellix-Regular',
  },

  txtDesc: {
    color: Colors.textHint,
    lineHeight: FontSizes.xl,
    fontWeight: '400',
    fontFamily: 'Hellix-Regular',
  },
});
