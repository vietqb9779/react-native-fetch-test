export interface IReward {
  cover: any;
  coin: number;
  content: string;
}

export interface IRewardItem {
  title: string;
  data: IReward[];
}
