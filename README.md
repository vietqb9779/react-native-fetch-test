# React Native Interview

A React Native Demo UI

# Libraries

- "react-native": "0.71.3"
- "react-native-svg": "13.8.0"

# Design

[Link Figma UI](https://www.figma.com/file/55YLBbjmmHjeZMx9mnxmtz/LUMENS---REACJS-TEST?node-id=0%3A1&t=qoN1KP4k6DxoZB04-0)

# Features

- Animation
- Font Hellix

# Demo

### iOS

|                                                                     |                                                                        |                                                                         |
| :-----------------------------------------------------------------: | :--------------------------------------------------------------------: | :---------------------------------------------------------------------: |
| <img width="1604" alt="screen shot" src="src/assets/docs/shot.gif"> | <img width="1604" alt="screen shot" src="src/assets/docs/shot001.png"> | <img width="1604" alt="screen shot " src="src/assets/docs/shot002.png"> |

### Android

|                                                                           |                                                                         |                                                                          |
| :-----------------------------------------------------------------------: | :---------------------------------------------------------------------: | :----------------------------------------------------------------------: |
| <img width="1604" alt="screen shot" src="src/assets/docs/gifAndroid.gif"> | <img width="1604" alt="screen shot" src="src/assets/docs/android1.png"> | <img width="1604" alt="screen shot " src="src/assets/docs/android2.png"> |

## Thank you

by Viet Nguyen 🇻🇳
